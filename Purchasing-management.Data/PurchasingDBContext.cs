﻿using Microsoft.EntityFrameworkCore;
using Purchasing_management.Common;
using Purchasing_management.Data.Entity;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Bogus;

namespace Purchasing_management.Data
{
    public class PurchasingDBContext: IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public PurchasingDBContext(DbContextOptions<PurchasingDBContext> options) : base(options)
        {
        }

        private readonly string connectionString;

        public PurchasingDBContext()
        {
            connectionString = Purchasing_management.Common.Utils.GetConfig("ConnectionStrings:PurchasingDBContext");
        }

        public virtual DbSet<Purchasing_management.Data.Entity.Department> Departments { get; set; }
        public virtual DbSet<Purchasing_management.Data.Entity.PurchaseOrder> PurchaseOrders { get; set; }
        public virtual DbSet<Purchasing_management.Data.Entity.Supply> Supplies { get; set; }
        public virtual DbSet<Purchasing_management.Data.Entity.SupplyWarehouse> SupplyWarehouse { get; set; }
        public virtual DbSet<Purchasing_management.Data.Entity.User> Users { get; set; }
        public virtual DbSet<Purchasing_management.Data.Entity.SummaryTable> SummaryTable { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            

            base.OnModelCreating(modelBuilder);
        }
    }

}
