﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Purchasing_management.Data.Entity
{
    public class SupplyWarehouse
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Amount { get; set; }

        public virtual ICollection<Supply> SupplyList { get; set; }
    }
}
