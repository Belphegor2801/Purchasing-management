﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Purchasing_management.Data;
using Purchasing_management.Data.Entity;
using Purchasing_management.Business;
using Purchasing_management.Common;


namespace Purchasing_management.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/statistic")]
    [ApiController]
    public class StatisticController: ControllerBase
    {
        private readonly StatisticHandler _statistictHandler;
        public StatisticController(StatisticHandler statisticHandler)
        {
            _statistictHandler = statisticHandler;
        }

        [HttpPost("employees/by-month")]
        public async Task<IActionResult> CountEmployeeByMonth()
        {
            var result = await _statistictHandler.CountEmployeeByMonth();
            return Helper.TransformData(result);
        }

        [HttpPost("employees/by-department")]
        public async Task<IActionResult> CountEmployeeByMonth(Guid departmentId)
        {
            var result = await _statistictHandler.CountEmployeeByDepartment();
            return Helper.TransformData(result);
        }

        [HttpPost("purchase-orders/by-month")]
        public async Task<IActionResult> CountPurchaseOrderByMonth(int year, int month)
        {
            var result = await _statistictHandler.CountPurchaseOrderByMonth(Convert.ToInt32(year), Convert.ToInt32(month));
            return Helper.TransformData(result);
        }
    }
}
