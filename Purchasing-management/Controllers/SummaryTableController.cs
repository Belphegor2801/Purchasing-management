﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Purchasing_management.Data;
using Purchasing_management.Data.Entity;
using Purchasing_management.Business;
using Purchasing_management.Common;

namespace Purchasing_management.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/summary_table")]
    [ApiController]
    public class SummaryTableController: ControllerBase
    {
        private readonly SummaryTableHandler _summaryTableHandler;
        public SummaryTableController(SummaryTableHandler summaryTableHandler)
        {
            _summaryTableHandler = summaryTableHandler;
        }

        [HttpGet("the-most-products/by-department")]
        public async Task<IActionResult> GetDepartmentHavingTheMostProducts()
        {
            var result = await _summaryTableHandler.GetDepartmentHavingTheMostProducts();
            return Helper.TransformData(result);
        }

        [HttpGet("the-most-purchase-orders/by-product")]
        public async Task<IActionResult> GetProductHavingTheMostRegister()
        {
            var result = await _summaryTableHandler.GetProductsHavingTheMostRegisters();
            return Helper.TransformData(result);
        }

        [HttpGet("the-most-employee-count/by-month")]
        public async Task<IActionResult> GetMonthHavingTheMostEmployeeCount()
        {
            var result = await _summaryTableHandler.GetMonthHavingTheMostEmployees();
            return Helper.TransformData(result);
        }

        [HttpGet("the-most-purchase-order/by-month")]
        public async Task<IActionResult> GetMonthHavingTheMostPurchaseOrders()
        {
            var result = await _summaryTableHandler.GetMonthHavingTheMostPurchaseOrders();
            return Helper.TransformData(result);
        }
    }
}
