﻿using Purchasing_management.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Purchasing_management.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeedController
    {
        private IConfiguration _config;
        private Seeding handler;

        public SeedController(IConfiguration config, Seeding handler)
        {
            _config = config;
            this.handler = handler;
        }

        // GET api/values
        [HttpGet]
        [Route("all")]
        public async Task<string> SeedAll()
        {
            await handler.SeedDepartments();

            await handler.SeedEmployees();

            await handler.SeedSuppleWareHouse();

            await handler.SeedPurchaseOrder();

            return "OK";
        }
    }
}
