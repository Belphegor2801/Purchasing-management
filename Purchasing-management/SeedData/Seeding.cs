﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Purchasing_management.Common;
using Purchasing_management.Business;
using Purchasing_management.Data;
using Purchasing_management.Data.Entity;
using Bogus;

namespace Purchasing_management.Api
{
    public class Seeding
    {
        private readonly PurchasingDBContext _dbcontext;
        private readonly DepartmentHandler _departmentHandler;
        private readonly PurchasingHandler _purchasingHandler;
        public Seeding(PurchasingDBContext dbContext, DepartmentHandler departmentHandler, PurchasingHandler purchasingHandler)
        {
            _dbcontext = dbContext;
            _departmentHandler = departmentHandler;
            _purchasingHandler = purchasingHandler;
        }

        public Faker faker = new Faker();

        public async Task SeedDepartments()
        {
            int num = 1;
            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Ban Điều Hành",
                Manager = "Nguyễn Tiến Sơn",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Ban Đào Tạo",
                Manager = "Ngô Xuân Hinh",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Ban Nhân Sự",
                Manager = "Lê Tuấn Việt",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Ban Giám Sát",
                Manager = "Hoàng Minh Chí",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Ban Truyền Thông",
                Manager = "Đỗ Tài Linh",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Ban Sự Kiện",
                Manager = "Ngụy Lê Minh Hiếu",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Ban Đời Sống",
                Manager = "Tiêu Thị Hoa",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Power Team Lập Trình",
                Manager = "Dương Văn Hữu",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Power Team Cơ Khí",
                Manager = "Ngô Đức",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });

            await _departmentHandler.AddDepartment(new DepartmentCreateModel
            {
                Name = "Power Team Tự Động Hóa & IOM",
                Manager = "Trần Duy Linh",
                CreatedDate = new DateTime(2021, 12, 29),
                OrdinalNumber = num++
            });
        }


        public async Task SeedEmployees()
        {
            var departments = _dbcontext.Departments as IQueryable<Department>;
            var departmentIds = departments.Select(x => x.Id).ToList();

            int n = 1000;
            var Guids = GenerateGuids(n + 1);

            for (int i = 0; i <n; i++)
                _dbcontext.Users.Add(new User()
                {
                    Id = Guids[i],
                    Name = faker.Name.FullName(),
                    AttendedDate = faker.Date.Between(DateTime.Today.AddMonths(-1), DateTime.Today),
                    DepartmentId = faker.PickRandom(departmentIds)
                });
            _dbcontext.SaveChanges();
        }

        public async Task SeedSuppleWareHouse()
        {
            int n = 1000;

            var Guids = GenerateGuids(n + 1);

            for (int i = 0; i <= n; i++)
                _dbcontext.SupplyWarehouse.Add(new SupplyWarehouse()
                {
                    Id = Guids[i],
                    Name = "Supply-" + i.ToString(),
                    Amount = faker.Random.Number(200, 500).ToString()
                });

            _dbcontext.SaveChanges();
        }

        public async Task SeedPurchaseOrder()
        {
            var users = _dbcontext.Users as IQueryable<User>;
            var employeeNames = users.Select(x => x.Name).ToList();
            var employeeIds = users.Select(x => x.Id).ToList();

            var supplyWarehouse = _dbcontext.SupplyWarehouse as IQueryable<SupplyWarehouse>;
            var suppyIds = supplyWarehouse.Select(x => x.Id).ToList();

            var departments = _dbcontext.Departments as IQueryable<Department>;
            var departmentIds = departments.Select(x => x.Id).ToList();

            int n = 1000;
            var Guids = GenerateGuids(n);
            var SupplyGuids = GenerateGuids(n * 10);
            for (int i = 0; i < n; i++)
            {
                _dbcontext.PurchaseOrders.Add(new PurchaseOrder()
                {
                    Id = Guids[i],
                    DepartmentId = faker.PickRandom(departmentIds),
                    RegistantName = faker.PickRandom(employeeNames),
                    CreateByUserId = faker.PickRandom(employeeIds),
                    CreatedDate = faker.Date.Between(DateTime.Today.AddMonths(-1), DateTime.Today)
                });

                for (int j = 0; j < faker.Random.Number(5, 10); j++)
                {
                    _dbcontext.Supplies.Add(new Supply()
                    {
                        Id = SupplyGuids[10 * i + j],
                        OrdinalNumber = j,
                        SupplyWareHouseId = faker.PickRandom(suppyIds),
                        Amount = faker.Random.Number(5, 10).ToString(),
                        OrderId = Guids[i]
                    });
                } 
            }
            _dbcontext.SaveChanges();
        }

        private List<Guid> GenerateGuids(int n)
        {
            var Guids = new List<Guid>();
            while (Guids.Count < n)
            {
                var newGuid = Guid.NewGuid();
                if (!Guids.Contains(newGuid))
                    Guids.Add(newGuid);
            }
            return Guids;
        }
    }
}
