﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Purchasing_management.Data;
using Purchasing_management.Data.Entity;
using Purchasing_management.Common;
using Purchasing_management.Business;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;

namespace Purchasing_management.BackgroundTask
{
    public class UpdateSummaryTableService: IHostedService, IDisposable
    {
        private int executionCount = 0;
        private readonly ILogger<UpdateSummaryTableService> _logger;
        private Timer? _timer = null;
        private readonly SummaryTableHandler _summaryTableHandler;
        private readonly PurchasingDBContext _dbcontext;

        public UpdateSummaryTableService(ILogger<UpdateSummaryTableService> logger, SummaryTableHandler summaryTableHandler, PurchasingDBContext purchasingDBContext)
        {
            _logger = logger;
            _summaryTableHandler = summaryTableHandler;
            _dbcontext = purchasingDBContext;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Background Task: UpdateSummaryTableService is running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(15));

            return Task.CompletedTask;
        }

        private void DoWork(object? state)
        {
            var result = _summaryTableHandler.getDepartmentHavingTheMostProducts();

            //Delete all data
            foreach (var row in _dbcontext.SummaryTable)
            {
                _dbcontext.SummaryTable.Remove(row);
            }
            _dbcontext.SaveChanges();

            // Thêm dữ liệu mới
            foreach (var supply in result.SupplyList)
            {
                _dbcontext.SummaryTable.Add(new SummaryTable()
                {
                    DepartmentId = result.Department.Id,
                    SupplyName = supply.Name,
                    Amount = Convert.ToInt32(supply.Amount)
                });
            }
            _dbcontext.SaveChanges();
            _logger.LogInformation("Update Summary Tabel.");
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Background Task: UpdateSummaryTableService is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
