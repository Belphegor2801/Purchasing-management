﻿using Purchasing_management.Data;
using Purchasing_management.Data.Entity;
using Purchasing_management.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using LinqKit;
using System.Linq.Expressions;
using System.Linq;
using AutoMapper;

namespace Purchasing_management.Business
{
    public class StatisticHandler
    {
        private readonly PurchasingDBContext _dbcontext;

        public StatisticHandler(PurchasingDBContext DBContext)
        {
            _dbcontext = DBContext ?? throw new ArgumentNullException(nameof(DBContext));
        }

        public async Task<Response> CountEmployeeByMonth()
        {
            var employeeCountDictionaryByMonth = new Dictionary<string, int>();

            foreach (var employee in _dbcontext.Users)
            {
                var key = employee.AttendedDate.Year.ToString() + "-" + employee.AttendedDate.Month.ToString();
                if (!employeeCountDictionaryByMonth.ContainsKey(key))
                    employeeCountDictionaryByMonth[key] = 1;
                else
                    employeeCountDictionaryByMonth[key] += 1;
            }

            var result = new List<EmployeeCountByMonth>();
            foreach (var key in employeeCountDictionaryByMonth.Keys)
            {
                result.Add(new EmployeeCountByMonth()
                {
                    Year = Convert.ToInt32(key.Split("-")[0]),
                    Month = Convert.ToInt32(key.Split("-")[1]),
                    Total = employeeCountDictionaryByMonth[key]
                });
            }

            return new ResponseList<EmployeeCountByMonth>(result);
        }

        public async Task<Response> CountEmployeeByDepartment()
        {
            var result = new List<EmployeeCountByDepartment>();

            foreach (var department in _dbcontext.Departments.Include(x => x.Employees))
            {
                result.Add(new EmployeeCountByDepartment()
                {
                    Department = department.Name,
                    Total = department.Employees.Count
                });
            }

            return new ResponseList<EmployeeCountByDepartment>(result);
        }

        public async Task<Response> CountPurchaseOrderByMonth(int year, int month)
        {
            var purchaseOrderCountDictionaryByMonth = new Dictionary<string, int>();

            foreach (var purchaseOrder in _dbcontext.PurchaseOrders)
            {
                var key = purchaseOrder.CreatedDate.Year.ToString() + "-" + purchaseOrder.CreatedDate.Month.ToString();
                if (!purchaseOrderCountDictionaryByMonth.ContainsKey(key))
                    purchaseOrderCountDictionaryByMonth[key] = 1;
                else
                    purchaseOrderCountDictionaryByMonth[key] += 1;
            }

            var result = new List<PurchaseOrderCountByMonth>();
            foreach (var key in purchaseOrderCountDictionaryByMonth.Keys)
            {
                result.Add(new PurchaseOrderCountByMonth()
                {
                    Year = Convert.ToInt32(key.Split("-")[0]),
                    Month = Convert.ToInt32(key.Split("-")[1]),
                    Total = purchaseOrderCountDictionaryByMonth[key]
                });
            }

            return new ResponseList<PurchaseOrderCountByMonth>(result);
        }
    }
}
