﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Purchasing_management.Business
{
    public class EmployeeCountByMonth
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Total { get; set; }
    }

    public class EmployeeCountByDepartment
    {
        public string Department { get; set; }
        public int Total { get; set; }
    }

    public class PurchaseOrderCountByMonth
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Total { get; set; }
    }
}
