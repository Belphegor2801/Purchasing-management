﻿using Purchasing_management.Data;
using Purchasing_management.Data.Entity;
using Purchasing_management.Common;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using LinqKit;
using System.Linq.Expressions;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Purchasing_management.Business
{
    public class SummaryTableHandler
    {
        private readonly PurchasingDBContext _dbcontext;
        private readonly IMapper _mapper;

        public SummaryTableHandler(PurchasingDBContext DBContext, IMapper mapper)
        {
            _dbcontext = DBContext ?? throw new ArgumentNullException(nameof(DBContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public ProductsByDepartment getDepartmentHavingTheMostProducts()
        {
            var productDictionaryByDepartment = new Dictionary<Department, Dictionary<string, int>>();

            // Tạo dictionary cho từng ban
            foreach (var department in _dbcontext.Departments.ToList())
            {
                productDictionaryByDepartment[department] = new Dictionary<string, int>();
            }
            // Tạo thư viện tính số lượng mỗi sản phầm của từng ban
            foreach (var purchaseOrder in _dbcontext.PurchaseOrders.Include(x => x.SupplyList).Include(x => x.Department).ToList())
            {
                foreach (var supply in purchaseOrder.SupplyList)
                {
                    var supplyName = _dbcontext.SupplyWarehouse.Find(supply.SupplyWareHouseId).Name;
                    if (!productDictionaryByDepartment[purchaseOrder.Department].ContainsKey(supplyName))
                        productDictionaryByDepartment[purchaseOrder.Department][supplyName] = Convert.ToInt32(supply.Amount);
                    else
                        productDictionaryByDepartment[purchaseOrder.Department][supplyName] += Convert.ToInt32(supply.Amount);
                }
            }

            // Trả về ban đăng ký nhiều sản phẩm nhất
            var departmentToReturn = new Department();

            int maxcount = 0;
            foreach (var department in productDictionaryByDepartment.Keys)
            {
                int count = productDictionaryByDepartment[department].Keys.Count;
                if (count > maxcount)
                {
                    maxcount = count;
                    departmentToReturn = department;
                }
            }

            // Danh sách sản phẩm của ban
            var supplyListToReturn = new List<SupplyWarehouse>();
            foreach (var supply in productDictionaryByDepartment[departmentToReturn])
            {
                supplyListToReturn.Add(new SupplyWarehouse()
                {
                    Name = supply.Key,
                    Amount = supply.Value.ToString()
                });
            }

            //result
            var result = new ProductsByDepartment()
            {
                Department= departmentToReturn,
                Total = maxcount,
                SupplyList = supplyListToReturn
            };

            return result;
        }

        public async Task<Response> GetDepartmentHavingTheMostProducts()
        {
            var result = this.getDepartmentHavingTheMostProducts();
            return new Response<ProductsByDepartmentDto>(_mapper.Map<ProductsByDepartmentDto>(result));
        }


        public async Task<Response> GetProductsHavingTheMostRegisters()
        {
            // SupplyWarehouse và danh sách 2 giá trị: Số đăng ký và tổng số sản phẩm
            var productDictionaryByResgister = new Dictionary<SupplyWarehouse, List<int>>();

            // Tạo dictionary cho từng Supply
            foreach (var supply in _dbcontext.SupplyWarehouse.ToList())
            {
                productDictionaryByResgister[supply] = new List<int>() { 0, 0 };
            }

            // Thống kê số lượng từng Supply
            foreach (var purchaseOrder in _dbcontext.PurchaseOrders.Include(x => x.SupplyList).Include(x => x.Department).ToList())
            {
                foreach (var supply in purchaseOrder.SupplyList)
                {
                    var supplyWarehouse = _dbcontext.SupplyWarehouse.Find(supply.SupplyWareHouseId);
                    productDictionaryByResgister[supplyWarehouse][0] += 1;
                    productDictionaryByResgister[supplyWarehouse][1] += Convert.ToInt32(supply.Amount);
                }
            }

            // Trả về supply được đăng ký nhiều nhất
            var supplyToReturn = new SupplyWarehouse();
            int maxcount = 0;
            int registerCount = 0;
            foreach (var key in productDictionaryByResgister.Keys)
            {
                if (productDictionaryByResgister[key][1] > maxcount)
                {
                    registerCount = productDictionaryByResgister[key][0];
                    maxcount = productDictionaryByResgister[key][1];
                    supplyToReturn = key;
                }
            }

            return new ResponseObject<SupplyCount>(new SupplyCount() {Name = supplyToReturn.Name, Amount = registerCount.ToString(), Total = maxcount.ToString()});
        }

        public async Task<Response> GetMonthHavingTheMostEmployees()
        {
            var employeeCountDictionaryByMonth = new Dictionary<string, int>();

            // Thống kê số lượng nhân viên theo tháng
            foreach (var employee in _dbcontext.Users.ToList())
            {
                var key = employee.AttendedDate.Year.ToString() + "-" + employee.AttendedDate.Month.ToString();

                if (!employeeCountDictionaryByMonth.ContainsKey(key))
                    employeeCountDictionaryByMonth[key] = 1;
                else
                    employeeCountDictionaryByMonth[key] += 1;
            }

            // Trả về tháng có số lượng nhân viên nhiều nhất
            var result = new EmployeeCountByMonth();

            int maxcount = 0;

            foreach(var key in employeeCountDictionaryByMonth.Keys)
            {
                if (employeeCountDictionaryByMonth[key] > maxcount)
                {
                    maxcount = employeeCountDictionaryByMonth[key];
                    result.Month = Convert.ToInt32(key.Split("-")[1]);
                    result.Year = Convert.ToInt32(key.Split("-")[0]);
                    result.Total = maxcount;
                }
            }

            return new ResponseObject<EmployeeCountByMonth>(result);
        }

        public async Task<Response> GetMonthHavingTheMostPurchaseOrders()
        {
            var purchaseOrderCountDictionaryByMonth = new Dictionary<string, Dictionary<string, int>>();

            // Thống kê số lượng supply đăng ký theo tháng
            foreach (var purchaseOrder in _dbcontext.PurchaseOrders.Include(x => x.SupplyList).ToList())
            {
                var key = purchaseOrder.CreatedDate.Year.ToString() + "-" + purchaseOrder.CreatedDate.Month.ToString();

                foreach (var supply in purchaseOrder.SupplyList)
                {
                    var supplyName = _dbcontext.SupplyWarehouse.Find(supply.SupplyWareHouseId).Name;
                    if (!purchaseOrderCountDictionaryByMonth.ContainsKey(key))
                    {
                        purchaseOrderCountDictionaryByMonth[key] = new Dictionary<string, int>();
                        if (!purchaseOrderCountDictionaryByMonth[key].ContainsKey(supplyName))
                            purchaseOrderCountDictionaryByMonth[key][supplyName] = Convert.ToInt32(supply.Amount);
                        else
                            purchaseOrderCountDictionaryByMonth[key][supplyName] += Convert.ToInt32(supply.Amount);
                    }

                    else
                    {
                        if (!purchaseOrderCountDictionaryByMonth[key].ContainsKey(supplyName))
                            purchaseOrderCountDictionaryByMonth[key][supplyName] = Convert.ToInt32(supply.Amount);
                        else
                            purchaseOrderCountDictionaryByMonth[key][supplyName] += Convert.ToInt32(supply.Amount);
                    }     
                }
            }

            // Trả về tháng có số lượng nhân viên nhiều nhất
            var result = new SupplyCountByMonth();

            int maxcount = 0;

            foreach (var time in purchaseOrderCountDictionaryByMonth.Keys)
            {
                int count = 0;
                foreach (var supply in purchaseOrderCountDictionaryByMonth[time].Keys)
                {
                    count += purchaseOrderCountDictionaryByMonth[time][supply];
                }

                if (count > maxcount)
                {
                    maxcount = count;
                    result.Month = Convert.ToInt32(time.Split("-")[1]);
                    result.Year = Convert.ToInt32(time.Split("-")[0]);
                    result.Total = maxcount;
                }
            }
            // Lấy danh sách supply
            result.Supplies = new List<SupplyDto>();
            var keyToReturn = result.Year.ToString() + "-" + result.Month.ToString();
            foreach (var supply in purchaseOrderCountDictionaryByMonth[keyToReturn])
            {
                result.Supplies.Add(new SupplyDto()
                {
                    Name = supply.Key,
                    Amount = supply.Value.ToString()
                });
            }

            return new ResponseObject<SupplyCountByMonth>(result);
        }
    }
}
