﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Purchasing_management.Data.Entity;

namespace Purchasing_management.Business
{
    public class ProductsByDepartment
    {
        public Department Department { get; set; }
        public int Total { get; set; }
        public List<SupplyWarehouse> SupplyList { get; set; }
    }

    public class ProductsByDepartmentDto
    {
        public string DepartmentName { get; set; }
        public int Total { get; set; }
        public List<SupplyWarehouseDto> SupplyList { get; set; }
    }

    public class SupplyCount
    {
        public string Name { get; set; }
        public string Amount { get; set; }
        public string Total { get; set; }
    }

    public class SupplyCountByMonth
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Total { get; set; }
        public List<SupplyDto> Supplies { get; set; }
    }
}
